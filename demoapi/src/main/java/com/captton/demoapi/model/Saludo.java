package com.captton.demoapi.model;

public class Saludo {
	//Defino las variables que voy a recibir del front-end, en nuestro caso del postman.
	private String remitente;
	private String destinatario;
	private String mensaje;
	
	//Getters and Setters
	public String getRemitente() {
		return remitente;
	}
	public void setRemitente(String remitente) {
		this.remitente = remitente;
	}
	public String getDestinatario() {
		return destinatario;
	}
	public void setDestinatario(String destinatario) {
		this.destinatario = destinatario;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	
	
	
}
