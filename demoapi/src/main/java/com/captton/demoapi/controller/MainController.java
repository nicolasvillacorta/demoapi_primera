package com.captton.demoapi.controller;


import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.captton.demoapi.model.Saludo;


@RestController
//@RequestMapping({"/consultas"})
public class MainController {

	
	@GetMapping
	public ResponseEntity<Object> saludar() {
		JSONObject obj = new JSONObject();
		
		try {
		obj.put("Error", "0");
		obj.put("Saludo", "Hola chicos!!!");
		} catch (JSONException e) {
	
		}
		
		return ResponseEntity.ok().body(obj.toString());
	}
	
	@GetMapping({"/mejorsaludo"})
	public String saludarMejor() {
		
		return "Hola, buen dia, como le va a usted?";
	}
	
	@GetMapping("/mejorsaludo2/{nom}")
	public String saludarMejor2(@PathVariable String nom) {
		
		return "Hola, buen dia "+nom+".";
	}
	
	// El post no anda en el navegador, lo tengo que probar en el Postman (programa).
	@PostMapping({"/guardar"})
	public ResponseEntity<Object> guardarSaludo(@RequestBody Saludo saludo,
												@RequestHeader(name = "Authorization", required = true) String headerAuth,
												@RequestHeader(name = "Otro", required = true) String headerOtro){
		
		System.out.println("Remitente: "+saludo.getRemitente());
		System.out.println("Destinatario: "+saludo.getDestinatario());
		System.out.println("Mensaje: "+saludo.getMensaje());
		
		System.out.println("Auth y el Otro: "+headerAuth+", "+headerOtro);
		
		JSONObject obj = new JSONObject();
		
		if(saludo.getDestinatario().contentEquals("Jose")) {
				
			try {
			obj.put("error", "0");
			obj.put("message", "Su mensaje ha sido guardado.");
			} catch (JSONException e) {
		
			}
			
			return ResponseEntity.ok().body(obj.toString());
			
		}else {
			
			try {
			obj.put("error", "1");
			obj.put("message", "Solo guardamos mensajes para Jose.");
			} catch (JSONException e) {
		
			}
			
			// Puedo devolver 404 not found. (Se ve solo en el postman esto, esto no puedo probarlo en el navegador, no olvidar)
			// return ResponseEntity.status(HttpStatus.NOT_FOUND).body(obj.toString());
			
			 return ResponseEntity.ok().body(obj.toString());
			
		}
		
	}


	
}
